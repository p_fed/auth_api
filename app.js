/**
 * @module app
 */
'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const db = require('./db');

const router = require('./router');

const app = express();

const port = process.env.PORT || 3000;

app.use(morgan('dev'));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

router(app);

app.listen(port, () => {
    console.log('Server has been started on ' + port + ' port');
});