# Poirot API

<a name="general"></a>
## Общая информация

<a name="requirements"></a>
### Требования

* [Docker](https://docs.docker.com/install/)
* [Docker compose](https://docs.docker.com/compose/install/)

<a name="install"></a>
### Установка

Для локальной разработки необходимо развернуть Docker контейнер командой
```
docker-compose up -d
```
В результате по адресу `http://localhost:3000/` запустится сервер приложения

## Описание API

### Регистрация пользователя

`POST /auth/register` 

Тело запроса 

```
{
    "first_name":"Ivan",
    "last_name":"Ivanov",
    "password":"pass",
    "email":"ivan.ivanov@gmail.com",
    "role":"admin"
}
```
### Ответ

Успешный ответ приходит с кодом `200 OK` и содержит тело:

```
{
    "auth": true,
    "userId": "5a8acaeac106ff008909e8dc",
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVhOGFjYWVhYzEwNmZmMDA4OTA5ZThkYyIsInJvbGUiOiJhZG1pbiIsImlhdCI6MTUxOTA0NTM1NCwiZXhwIjoxNTE5MTMxNzU0fQ.E56XjwYQsa7fLbf56lWMLyWmnhHwQeqF4y15Sn2gdG8",
    "message": "User ivan.ivanov@gmail.com has been created"
}
```

### Аутентификация пользователя

`POST /auth/login` 

Тело запроса 

```
{
    "first_name":"Ivan",
    "last_name":"Ivanov",
    "password":"pass",
    "email":"ivan.ivanov@gmail.com",
    "role":"admin"
}
```

Успешный ответ приходит с кодом `200 OK` и содержит тело:


```
{
    "auth": true,
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVhODgyYWRiNGJiNzU5MjEzOGZkNzMxMSIsImlhdCI6MTUxODg3OTYzOCwiZXhwIjoxNTE4OTY2MDM4fQ.4hnrx5aUblziT3OCBOO497snNorXq6EBMa5UPe6ccy4"
}
```

### Использование и проверка токена

Приложение должно использовать полученный `token` для авторизации, передавая его в заголовке в формате:

``
x-access-token: token
``

Для тестирования токена, удобно использовать метод `auth/me`.

```http
GET /auth/me HTTP/1.1
Host: localhost:3000
x-access-token: token
```
### Ответ

Успешный ответ приходит с кодом `200 OK` и содержит тело:
```
{
    "_id": "5a8acaeac106ff008909e8dc",
    "firstName": "Ivan",
    "lastName": "Ivanov",
    "email": "ivan.ivanov@gmail.com",
    "role": "admin",
    "__v": 0
}
```