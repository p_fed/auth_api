/**
 * @module index
 */
'use strict';

const express = require('express');
const router = express.Router();

const controller  = require('./auth.controller');
const verifyToken  = require('./verifyToken');

router.post('/register', controller.register);
router.post('/login', controller.login);
router.get('/me', verifyToken, controller.me);

module.exports = router;

