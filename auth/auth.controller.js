/**
 * @module auth.controller
 */
'use strict';
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const config = require('../config');

const User = require('../model/user');

exports.register = (req, res) => {
    const hashedPassword = bcrypt.hashSync(req.body.password, 8);
    User.create({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        role: req.body.role,
        password: hashedPassword
    })
        .then((user) => {
            const token = jwt.sign({
                id: user._id,
                role: user.role
            }, config.secret, {
                expiresIn: 86400 // expires in 24 hours
            });

            res.send({
                auth: true,
                userId: user.id,
                token: token,
                message: 'User ' + user.email + ' has been created'
            });
        })
        .catch((err) => {
            res.send(err);
        });
};

exports.login = (req, res) => {
    User.findOne({email: req.body.email})
        .then((user) => {
                if (!user) {
                    return res.status(404).send({error: 'User not found'});
                }
                const passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
                if (!passwordIsValid) {
                    return res.status(401).send({auth: false, token: null});
                }
                const token = jwt.sign({id: user._id}, config.secret, {
                    expiresIn: 86400 // expires in 24 hours
                });
                res.send({auth: true, token: token});
            }
        )
        .catch((err) => {
            res.send(err);
        });
};

exports.me = (req, res) => {
    User.findById(req.userId, {password: 0})
        .then((user) => {
            if (!user) {
                return res.status(404).send({error: 'User not found'});
            }
            res.status(200).send(user);
        });
};